package com.nexos.nexosSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexosSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(NexosSpringApplication.class, args);
	}

}
