/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.controllers;

import com.nexos.nexosSpring.models.entity.Auditoria;
import com.nexos.nexosSpring.models.entity.Persona;
import com.nexos.nexosSpring.models.service.IPersonaService;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author flexiweb
 */
@RestController
@RequestMapping("/api")
public class PersonaRestController {

    @Autowired
    private IPersonaService personaService;

    InetAddress IP = null;
    Auditoria auditoria = new Auditoria();

    @GetMapping("/personas")
    public List<Persona> index() {
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PersonaRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        auditoria.setIp(IP.getHostAddress());
        auditoria.setAccion("CONSULTA MASIVA");
        auditoria.setDatos_respuesta("Busca todos las personas");
        personaService.save(auditoria);
        return personaService.findAll();
    }

    @GetMapping("/personas/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {

        Persona persona = null;
        Map<String, Object> response = new HashMap<>();

        try {
            persona = personaService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (persona == null) {
            response.put("mensaje", "El persona ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PersonaRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        auditoria.setIp(IP.getHostAddress());
        auditoria.setAccion("CONSULTA POR CC");
        auditoria.setDatos_respuesta(persona.getNombres() + " " + persona.getApellidos() + " " + persona.getCedula()
                + " " + persona.getEmail() + " " + persona.getTelefono() + " " + persona.getTipo_nucleo_familiar());
        personaService.save(auditoria);
        return new ResponseEntity<Persona>(persona, HttpStatus.OK);
    }

    @PostMapping("/personas")
    public ResponseEntity<?> create(@Valid @RequestBody Persona persona, BindingResult result) {

        Persona personaNew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {

            List<String> errors = result.getFieldErrors().stream()
                    .map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            personaNew = personaService.save(persona);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "La persona ha sido creado con éxito!");
        response.put("persona", personaNew);
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PersonaRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        auditoria.setIp(IP.getHostAddress());
        auditoria.setAccion("AGREGA PERSONA");
        auditoria.setDatos_respuesta(persona.getNombres() + " " + persona.getApellidos() + " " + persona.getCedula()
                + " " + persona.getEmail() + " " + persona.getTelefono() + " " + persona.getTipo_nucleo_familiar());
        personaService.save(auditoria);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @PutMapping("/personas/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Persona persona, BindingResult result, @PathVariable Long id) {

        Persona personaActual = personaService.findById(id);

        Persona personaUpdated = null;

        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {

            List<String> errors = result.getFieldErrors().stream()
                    .map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        if (personaActual == null) {
            response.put("mensaje", "Error: no se pudo editar, la Persona ID: "
                    .concat(id.toString().concat(" no existe en la base de datos!")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        try {
            personaActual.setNombres(persona.getNombres());
            personaActual.setApellidos(persona.getApellidos());
            personaActual.setEmail(persona.getEmail());
            personaActual.setTelefono(persona.getTelefono());
            personaActual.setTipo_nucleo_familiar(persona.getTipo_nucleo_familiar());
            personaUpdated = personaService.save(personaActual);

        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar el persona en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "La persona ha sido actualizado con éxito!");
        response.put("persona", personaUpdated);

        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PersonaRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        auditoria.setIp(IP.getHostAddress());
        auditoria.setAccion("ACTUALIZA PERSONA");
        auditoria.setDatos_respuesta("Antes: " + persona.getNombres() + " " + persona.getApellidos() + " " + persona.getCedula()
                + " " + persona.getEmail() + " " + persona.getTelefono() + " " + persona.getTipo_nucleo_familiar() + " Despues: " + personaActual.getNombres() + " " + personaActual.getApellidos() + " " + personaActual.getCedula()
                + " " + personaActual.getEmail() + " " + personaActual.getTelefono() + " " + personaActual.getTipo_nucleo_familiar());
        personaService.save(auditoria);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/personas/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Persona persona = null;
        Map<String, Object> response = new HashMap<>();
        try {
            persona = personaService.findById(id);
            personaService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar la persona de la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "La persona eliminado con éxito!");
        try {
            IP = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PersonaRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        auditoria.setIp(IP.getHostAddress());
        auditoria.setAccion("BORRA PERSONA");
        auditoria.setDatos_respuesta("Persona a eliminar: " + persona.getNombres() + " " + persona.getApellidos() + " " + persona.getCedula()
                + " " + persona.getEmail() + " " + persona.getTelefono() + " " + persona.getTipo_nucleo_familiar());
        personaService.save(auditoria);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }
}
