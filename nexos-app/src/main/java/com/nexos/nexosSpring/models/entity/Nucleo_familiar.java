/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.models.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author flexiweb
 */
@Entity
@Table(name = "nucleo_familiar")
public class Nucleo_familiar implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_nucleo_familiar")
    private int id_nucleo_familiar;
    private String descripcion;
    
    /**
     * @return the id_nucleo_familiar
     */
    public int getId_nucleo_familiar() {
        return id_nucleo_familiar;
    }

    /**
     * @param id_nucleo_familiar the id_nucleo_familiar to set
     */
    public void setId_nucleo_familiar(int id_nucleo_familiar) {
        this.id_nucleo_familiar = id_nucleo_familiar;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
