/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author flexiweb
 */
@Entity
@Table(name = "persona")
public class Persona implements Serializable {

    @Id
    private Long cedula;
    private String nombres;
    private String apellidos;
    private String email;
    private int telefono;

    private int tipo_nucleo_familiar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_nucleo_familiar", referencedColumnName = "id_nucleo_familiar", insertable = false, updatable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Nucleo_familiar nucleo_familiar;

    /**
     * @return the cedula
     */
    public Long getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the tipo_nucleo_familiar
     */
    public int getTipo_nucleo_familiar() {
        return tipo_nucleo_familiar;
    }

    /**
     * @param tipo_nucleo_familiar the tipo_nucleo_familiar to set
     */
    public void setTipo_nucleo_familiar(int tipo_nucleo_familiar) {
        this.tipo_nucleo_familiar = tipo_nucleo_familiar;
    }

    public Nucleo_familiar getNucleo_familiar() {
        return nucleo_familiar;
    }

    public void setNucleo_familiar(Nucleo_familiar nucleo_familiar) {
        this.nucleo_familiar = nucleo_familiar;
    }
}
