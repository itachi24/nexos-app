/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.models.service;

import com.nexos.nexosSpring.models.dao.IAuditoriaDao;
import com.nexos.nexosSpring.models.dao.IPersonaDao;
import com.nexos.nexosSpring.models.entity.Auditoria;
import com.nexos.nexosSpring.models.entity.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author flexiweb
 */
@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private IPersonaDao personaDao;

    @Autowired
    private IAuditoriaDao auditoriaDao;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Transactional(readOnly = true)
    @Override
    public List<Persona> findAll() {
        return (List<Persona>) personaDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Persona findById(Long id) {
        return personaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Persona save(Persona persona) {
        return personaDao.save(persona);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        personaDao.deleteById(id);
    }

    @Override
    @Transactional
    public Long save(Auditoria auditoria) {
        return generaAuditoria(auditoria);
    }

    public Long generaAuditoria(Auditoria auditoria) {
        EntityManager em = emf.createEntityManager();
        final List result = new ArrayList<>();
        Session session = em.unwrap(Session.class);
        Long resultado = null;
        session.doWork(new Work() {

            @Override
            public void execute(Connection con) throws SQLException {
                System.out.println("con>>> " + con.getClass());
                try (PreparedStatement stmt = con.prepareStatement("call GRABA_AUDITORIA(?, ?, ?)")) {
                    stmt.setString(1, auditoria.getIp());
                    stmt.setString(2, auditoria.getAccion());
                    stmt.setString(3, auditoria.getDatos_respuesta());
                    stmt.execute();
                    stmt.close();
                }
            }

        });
        em.close();
        for (int i = 0; i < result.size(); i++) {
            Long result1 = Long.valueOf((String) result.get(i));
            resultado = result1;
        }
        return resultado;
    }
}
