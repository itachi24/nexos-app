/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.models.dao;

import com.nexos.nexosSpring.models.entity.Auditoria;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author flexiweb
 */
public interface IAuditoriaDao extends JpaRepository<Auditoria, Long>{
    
}
