/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.models.service;

import com.nexos.nexosSpring.models.entity.Auditoria;
import com.nexos.nexosSpring.models.entity.Persona;
import java.util.List;

/**
 *
 * @author flexiweb
 */
public interface IPersonaService {

    public List<Persona> findAll();

    public Persona findById(Long id);

    public Persona save(Persona persona);

    public void delete(Long id);
    
    public Long save(Auditoria auditoria);
    

}
