/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.nexosSpring.models.dao;

import com.nexos.nexosSpring.models.entity.Persona;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author flexiweb
 */
public interface IPersonaDao extends JpaRepository<Persona, Long> {
    
}
